package com.example.starpushandoridexample;

import android.widget.Toast;

import com.starpush.android.sdk.RemoteMessage;
import com.starpush.android.sdk.StarpushService;

public class PushService extends StarpushService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Toast.makeText(getApplicationContext(), "Got a push message: " + remoteMessage.getPayload(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String token) {
        Toast.makeText(getApplicationContext(), "Got a new token: " + token, Toast.LENGTH_SHORT).show();
    }
}
